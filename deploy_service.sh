echo init deploy

temp="project_dir"

echo installing java
if type -p java; then
    echo found java executable in PATH
    _java=java
else
    echo "no java found"
fi

if [[ "$_java" ]]; then
    version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    echo found version "$version"
    short_version=${version:0:3}
    if [[ "$short_version" == "1.8" ]]; then
        echo java version "$short_version" already installed
    else         
        echo version "$short_version" installing..
        sudo add-apt-repository ppa:webupd8team/java
        sudo apt-get update
	sudo apt-get install oracle-java8-installer
	JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/"
    fi
fi

if type -p git; then
    echo found git executable in PATH
    _git=git
    git_version=$("$_git" --version 2>&1 | awk -F '"' '/version/ {print $1}')
    echo "$git_version" already installed
else
    echo "no git found"
    echo git installing..
    sudo apt-get install git-all
    git -version
fi

if type -p mvn; then
    echo found mvn executable in PATH
    _mvn=mvn
    echo maven already installed
else
    echo "no mvn found"
    echo git installing..
    sudo apt-get install maven
    mvn -version
fi

sudo apt install tomcat8
sudo apt-get install tomcat8-docs tomcat8-examples tomcat8-admin
#sudo groupadd tomcat
#sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
#wget http://ftp.byfly.by/pub/apache.org/tomcat/tomcat-8/v8.5.27/bin/apache-tomcat-8.5.27.tar.gz
#sudo tar -xzvf apache-tomcat-8.5.27.tar.gz
#sudo mv apache-tomcat-8.5.27 /opt/tomcat
#sudo chgrp -R tomcat /opt/tomcat
#sudo chown -R tomcat /opt/tomcat
#sudo chmod -R 755 /opt/tomcat
# /opt/tomcat/bin/startup.sh

sudo systemctl start tomcat8
#systemctl stop tomcat8
#systemctl restart tomcat8

# ?????????????????

cd "/home/$USER"
mkdir java
cd java
git clone https://z80code@bitbucket.org/z80code/webservices.git
cd webservices
mvn clean deploy

